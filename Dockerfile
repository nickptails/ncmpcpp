ARG ALPINE_VERSION

FROM alpine:${ALPINE_VERSION} as builder

ARG NCMPCPP_VERSION

RUN apk add --no-cache \
        boost-dev \
        build-base \
        curl \
        curl-dev \
        fftw-dev \
        libmpdclient-dev \
        openssl1.1-compat-dev \
        ncurses-dev \
        readline-dev \
        taglib-dev \
        tar && \
    curl -fLo ncmpcpp-${NCMPCPP_VERSION}.tar.bz2 \
        http://rybczak.net/ncmpcpp/stable/ncmpcpp-${NCMPCPP_VERSION}.tar.bz2 && \
    tar xjf ncmpcpp-${NCMPCPP_VERSION}.tar.bz2 && \
    cd ncmpcpp-${NCMPCPP_VERSION} && \
    ./configure \
        BOOST_LIB_SUFFIX=-mt \
        --enable-clock \
        --with-fftw \
        --with-taglib \
        --enable-visualizer && \
    make && make DESTDIR=/ncmpcpp install


FROM alpine:${ALPINE_VERSION}

COPY --from=builder /ncmpcpp/usr/local/bin/ /usr/local/bin/
RUN apk add --no-cache \
        boost1.78-filesystem \
        boost1.78-locale \
        boost1.78-program_options \
        boost1.78-thread \
        fftw-double-libs \
        icu-libs \
        libcurl \
        libgcc \
        libmpdclient \
        libstdc++ \
        musl \
        ncurses-libs \
        ncurses-terminfo \
        readline \
        taglib

CMD ["ncmpcpp"]
